"use strict";

var laatstOpDeSite = localStorage.getItem('laatstOpDeSite');
var uitvoer = 'Dit is de eerste keer dat je op deze site komt.';

var setDate = function() {
    localStorage.setItem('laatstOpDeSite', new Date());
}

var reset = function() {
    localStorage.removeItem('laatstOpDeSite');
    window.location.reload();
}

if (laatstOpDeSite !== null) {
    uitvoer = 'Je was hier de laatste keer op: ' + laatstOpDeSite;
}
setDate();

document.getElementById('output').innerText = uitvoer;
document.getElementById('btnVerwijderMij').addEventListener('click', reset);